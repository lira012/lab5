import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  _showSnackBar() {
    final snackBar = new SnackBar(
      content: new Text('Oops! Something went wrong!'),
      duration: new Duration(seconds: 10),
      action: SnackBarAction(
        label: 'Dismiss',
        onPressed: () {},
      ),
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30))),
      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Lab 5",
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            title: Text("Avengers Assemble"),
            automaticallyImplyLeading: false,
            backgroundColor: Color(0xFFEF5350),
            bottom: TabBar(
              indicatorColor: Colors.white,
              tabs: [
                Tab(text: 'Missions', icon: Icon(Icons.list_alt)),
                Tab(text: 'Hero Profile', icon: Icon(Icons.account_circle)),
                Tab(text: 'Discover Allies', icon: Icon(Icons.supervised_user_circle)),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Container(
                child: ListView(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        new Container(
                          height: 250.0,
                          color: Colors.white,
                          child: new Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top: 20.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(left: 100.0),
                                      child: new Text('MISSION REPORT:\nOperation: Chitauri',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20.0,
                                        color: Colors.black),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 20.0),
                                child: new Stack(fit: StackFit.loose, children: <Widget>[
                                  new Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Container(
                                        width: 350.0,
                                        height: 160.0,
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          image: new DecorationImage(
                                            image: new ExactAssetImage('images/invasion.jpg'),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ]),
                              ),
                            ],
                          ),
                        ),
                        new Container(
                          color: Colors.white,
                          child: Padding(
                            padding: EdgeInsets.only(bottom: 30),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(
                                    left: 25, right: 25, top: 10),
                                  child: new Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          new Text(
                                            'Mission Description:',
                                            style: TextStyle(
                                              fontSize: 20.0,
                                              fontWeight: FontWeight.bold),
                                          ),
                                          new Text(
                                            'The Chitauri have invaded New York City in an attempt to take over the'
                                            'the world. You must team up with allies to prevent the Chitauri from being'
                                            'victorious. The world needs you Avenger!',
                                            style: TextStyle(
                                              fontSize: 15,
                                              color: Colors.black54,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        new Container(
                          color: Colors.white,
                          child: Padding(
                            padding: EdgeInsets.only(bottom: 30),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Tooltip(
                                  message: 'Shapeshifting Powers by Consumption',
                                  child: ElevatedButton(
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all<Color>(Colors.blue),
                                    ),
                                    onPressed: () {},
                                    child: Text('Powers'),
                                  ),
                                ),
                                Tooltip(
                                  message: 'Humanoid Creature with Protective Armor',
                                  child: ElevatedButton(
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all<Color>(Colors.deepPurple),
                                    ),
                                    onPressed: () {},
                                    child: Text('Structure'),
                                  ),
                                ),
                                Tooltip(
                                  message: 'Head/Chest and Destruction of Homebase',
                                  child: ElevatedButton(
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all<Color>(Colors.amber),
                                    ),
                                    onPressed: () {},
                                    child: Text('Weaknesses'),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        new Container(
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                            ),
                            onPressed: _showSnackBar,
                            child: Text('Accept Mission'),
                          ),
                        ),
                        new Container(
                          child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
                            ),
                            onPressed: _showSnackBar,
                            child: Text('Deny Mission'),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                child: ListView(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        new Container(
                          height: 250.0,
                          color: Colors.white,
                          child: new Column(
                            children: <Widget>[
                              Padding(
                                padding:EdgeInsets.only(top: 10.0),
                                child: new Stack(fit: StackFit.loose, children: <Widget>[
                                  new Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      new Container(
                                        width: 150,
                                        height: 150,
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: new DecorationImage(
                                            image: new ExactAssetImage('images/ant.jpg'),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      new Container(
                                        child: Text('Ant-Man',
                                            style: TextStyle(fontWeight: FontWeight.bold,
                                                fontSize: 30,
                                                color: Colors.black)),
                                      ),
                                    ],
                                  ),
                                ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Container(
                          color: Colors.white,
                          child: Padding(
                            padding: EdgeInsets.only(bottom: 1),
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 25,
                                      right: 25,
                                      top: 1),
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          new Text('Information',
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 25,
                                      right: 25,
                                      top: 1),
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          new Text('Email:',
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 25,
                                      right: 25,
                                      top: 1),
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Flexible(
                                        child: new TextField(
                                          decoration: const InputDecoration(
                                            hintText: "Enter Your Email",
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 25,
                                      right: 25,
                                      top: 1),
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          new Text('Phone Number:',
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 25,
                                      right: 25,
                                      top: 1),
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Flexible(
                                        child: new TextField(
                                          decoration: const InputDecoration(
                                            hintText: "Enter Your Phone Number",
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 25,
                                      right: 25,
                                      top: 1),
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          new Text('Powers:',
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 25,
                                      right: 25,
                                      top: 1),
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      new Flexible(
                                        child: new TextField(
                                          decoration: const InputDecoration(
                                            hintText: "Enter Your Powers",
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        new Container(
                          child: ElevatedButton(
                            onPressed: _showSnackBar,
                            child: Text('Save'),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              ListView(
                children: <Widget>[
                  Card(
                    color: Colors.white,
                    elevation: 10,
                    child: ListTile(
                      leading: CircleAvatar(radius: 25, backgroundImage: new AssetImage('images/thanos.jpg'),),
                      title: Text('Thanos'),
                      subtitle: Text('Mutual Friends: Iron-Man + 14 more'),
                      trailing: Icon(Icons.more_vert),
                      isThreeLine: true,
                    ),
                  ),
                  Card(
                      color: Colors.white,
                      elevation: 10,
                      child: ListTile(
                        leading: CircleAvatar(radius: 25, backgroundImage: new AssetImage('images/ironman.jpg'),),
                        title: Text('Iron-Man'),
                        subtitle: Text('Mutual Friends: Captain America + 12 more'),
                        trailing: Icon(Icons.more_vert),
                        isThreeLine: true,
                      ),
                  ),
                  Card(
                      color: Colors.white,
                      elevation: 10,
                      child: ListTile(
                        leading: CircleAvatar(radius: 25, backgroundImage: new AssetImage('images/cap.jpg'),),
                        title: Text('Captain America'),
                        subtitle: Text('Mutual Friends: Hulk + 5 more'),
                        trailing: Icon(Icons.more_vert),
                        isThreeLine: true,
                      ),
                  ),
                  Card(
                      color: Colors.white,
                      elevation: 10,
                      child: ListTile(
                        leading: CircleAvatar(radius: 25, backgroundImage: new AssetImage('images/hulk.jpg'),),
                        title: Text('Hulk'),
                        subtitle: Text('Mutual Friends: Black Widow + 20 more'),
                        trailing: Icon(Icons.more_vert),
                        isThreeLine: true,
                      ),
                  ),
                  Card(
                      color: Colors.white,
                      elevation: 10,
                      child: ListTile(
                        leading: CircleAvatar(radius: 25, backgroundImage: new AssetImage('images/widow.jpg'),),
                        title: Text('Black Widow'),
                        subtitle: Text('Mutual Friends: Hawkeye + 19 more'),
                        trailing: Icon(Icons.more_vert),
                        isThreeLine: true,
                      ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Tooltip(
                        message: 'Settings',
                        child: IconButton(
                          icon: Icon(Icons.pending),
                          onPressed: () {},
                        ),
                      ),
                      Tooltip(
                        message: 'Info',
                        child: IconButton(
                          icon: Icon(Icons.format_align_center),
                          onPressed: () {},
                        ),
                      ),
                      Tooltip(
                        message: 'Alert',
                        child: IconButton(
                          icon: Icon(Icons.add_alert),
                          onPressed: () {},
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

